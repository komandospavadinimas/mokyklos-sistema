﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;

[ServiceContract(Namespace = "")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class DatabaseService
{
    private SqlConnection GetConnectionContext()
    {
        var connectionString = ConfigurationManager.ConnectionStrings["MokyklosISEntities"].ConnectionString;
        var builder = new EntityConnectionStringBuilder(connectionString);

        return new SqlConnection(builder.ProviderConnectionString);
    }

    [OperationContract]
	public List<Vartotojas> GetVartotojai()
	{
        using (var context = new MokyklosISEntities())
            return context.Vartotojas.ToList();
	}

    [OperationContract]
    public List<Pazymys> GetGrades(int userID, int classID)
    {
        using (var context = new MokyklosISEntities())
        {
            var query = from grade in context.Pazymys where grade.Mokinio_ID == userID && grade.Modulio_ID == classID orderby grade.Data select grade;
            return query.ToList();
        }
    }

    [OperationContract]
    public void SaveAttendanceRecords(List<Lankomumas> param)
    {
        using(var context = new MokyklosISEntities())
        {
            foreach(var record in param)
            {
                var existingRec = context.Lankomumas.FirstOrDefault(o => o.Iraso_ID == record.Iraso_ID);

                if (existingRec == null)
                    continue;

                existingRec.Komentaras = record.Komentaras;
                existingRec.Ar_Pateisinta = record.Ar_Pateisinta;
            }

            context.SaveChanges();

            var afterChange = context.Lankomumas;
        }
    }

    [OperationContract]
    public Dictionary<int, string> GetClassNames(List<Lankomumas> param)
    {
        if (param == null)
            return null;

        var names = new Dictionary<int, string>();

        using (var context = new MokyklosISEntities())
        {
            foreach (var record in param)
            {
                var query = from lesson in context.Modulis where lesson.Modulio_ID == record.Modulio_ID select lesson;
                var module = query.ToList().FirstOrDefault();

                if (module != null)
                    names.Add(module.Modulio_ID, module.Pavadinimas);
            }

            return names;
        }
    }

    [OperationContract]
    public List<Lankomumas> GetAttendance(int childID, DateTime beginDate, DateTime endDate)
    {
        using(var context = new MokyklosISEntities())
        {
            var query = from record in context.Lankomumas where record.Mokinio_ID == childID && record.Data > beginDate && record.Data < endDate select record;
            var _return = query.ToList();
            return _return;
        }
    }

    [OperationContract]
    public List<Vartotojas> GetChildrenList(int parentID)
    {
        using(var context  = new MokyklosISEntities())
        {
            var cpQueury = from cp in context.Tevai_Vaikai where cp.Tevo_ID == parentID select cp;

            var childrenList = new List<Vartotojas>();

            foreach (var cp in cpQueury.ToList())
            {
                var childQuery = from child in context.Vartotojas where child.Vartotojo_ID == cp.Vaiko_ID select child;
                childrenList.AddRange(childQuery.ToList());
            }

            return childrenList;
        }
    }

    [OperationContract]
    public List<Vartotojas> GetVartotojai2()
    {
        using(var context = new MokyklosISEntities())
        {
            var query =
            from vart in context.Vartotojas
            where vart.Lytis == true
            orderby vart.Vartotojo_ID
            select vart;

            return query.ToList();
        }
    }

    [OperationContract]
    public List<Modulis> GetModuliai(int Id)
    {
        using (var context = new MokyklosISEntities())
        {
            var query1 = from mod in context.Mokytojai_Moduliai where mod.Mokytojo_ID == Id select mod;

            var modList = new List<Modulis>();

            foreach (var item in query1.ToList()) 
            {
                var query2 = from mod in context.Modulis where mod.Modulio_ID == item.Modulio_ID select mod;
                modList.AddRange(query2.ToList());
            }
            return modList;
        }
    }

    [OperationContract]
    public Vartotojas GetVartotoja(int Id)
    {
        using (var context = new MokyklosISEntities())
        {
            var query =
            from vart in context.Vartotojas
            where vart.Vartotojo_ID == Id
            select vart;

            return query.ToList().FirstOrDefault();
        }
    }

    [OperationContract]
    public Vartotojas IsCorrectLogin(string login, string pass)
    {
        using (var context = new MokyklosISEntities())
            return CheckLoginForContext(context, login, pass);
    }

    [OperationBehavior]
    public Vartotojas CheckLoginForContext(MokyklosISEntities context, string login, string pass)
    {
        var query = from v in context.Vartotojas where (v.Prisijungimo_Vardas == login && v.Slaptazodis == pass) select v;
        var list = query.ToList();
        return query.Count() == 1 ? list.First() : null;
    }

    [OperationBehavior]
    public string PickLogin(MokyklosISEntities context, string name, string lastname)
    {
        string baseString = name.Substring(0, 3) + lastname.Substring(0, 3);
        int add = 0;
        string finalString = baseString;

        for (int i = 0; i < 1000; i++)
        {
            var query = from user in context.Vartotojas where user.Prisijungimo_Vardas == finalString select user;
            var list = query.ToList();

            if (list.Count > 0)
            {
                add++;
                finalString = baseString + add.ToString();
            }
            else break;
        }

        return finalString;
    }

    [OperationContract]
    public bool CreateRelationShip_ParentChild(int parentID, int childID)
    {
        using(var context = new MokyklosISEntities())
        {
            var relationship = new Tevai_Vaikai();
            relationship.Tevo_ID = parentID;
            relationship.Vaiko_ID = childID;

            try
            {
                context.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
    }

    [OperationContract]
    public int CreateParent(Vartotojas administrator, Vartotojas parent, List<int> childrenList)
    {
        using (var context = new MokyklosISEntities())
        {
            if (CheckLoginForContext(context, administrator.Prisijungimo_Vardas, administrator.Slaptazodis) != null)
            {
                parent.Prisijungimo_Vardas = PickLogin(context, parent.Vardas, parent.Pavarde);
                parent.El_Pastas = string.Empty;
                parent.Adresas = string.Empty;
                context.Vartotojas.Add(parent);

                var parentData = new Tevai();

                try
                {
                    context.SaveChanges();
                    parent.Vartotojo_ID = parent.Vartotojo_ID;

                    foreach (var child in childrenList)
                    {
                        var relationship = new Tevai_Vaikai();
                        relationship.Tevo_ID = parent.Vartotojo_ID;
                        relationship.Vaiko_ID = child;
                        context.Tevai_Vaikai.Add(relationship);
                    }

                    context.SaveChanges();

                    return parent.Vartotojo_ID;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
        }

        return -1;
    }

    [OperationContract]
    public int AddAttendance(Lankomumas lank)
    {
        using (var context = new MokyklosISEntities())
        {
            context.Lankomumas.Add(lank);

            try
            {
                context.SaveChanges();

                return lank.Iraso_ID;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }
    }

    [OperationContract]
    public int AddNote(Pastaba past)
    {
        using (var context = new MokyklosISEntities())
        {
            context.Pastaba.Add(past);

            try
            {
                context.SaveChanges();

                return past.Pastabos_ID;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }
    }

    [OperationContract]
    public int AddGrade(Pazymys paz)
    {
        using (var context = new MokyklosISEntities())
        {
            context.Pazymys.Add(paz);

            try
            {
                context.SaveChanges();

                return paz.Pazymio_ID;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }
    }

    [OperationContract]
    public int CreateMessage(Pranesimas message)
    {
        using (var context = new MokyklosISEntities())
        {
            context.Pranesimas.Add(message);

            try
            {
                context.SaveChanges();

                return message.Pranesimo_ID;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }
    }

    [OperationContract]
    public int CreateStudent(Vartotojas administrator, Vartotojas student, Mokinys studentData)
    {
        using (var context = new MokyklosISEntities())
        {
            if (CheckLoginForContext(context, administrator.Prisijungimo_Vardas, administrator.Slaptazodis) != null)
            {
                student.Prisijungimo_Vardas = PickLogin(context, student.Vardas, student.Pavarde);
                student.El_Pastas = string.Empty;
                student.Adresas = string.Empty;
                context.Vartotojas.Add(student);

                try
                {
                    context.SaveChanges();

                    studentData.Mokinio_ID = student.Vartotojo_ID;
                    context.Mokinys.Add(studentData);

                    context.SaveChanges();

                    return student.Vartotojo_ID;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
        }

        return -1;
    }

    [OperationContract]
    public int CreateUser(Vartotojas administrator, Vartotojas newUser)
    {
        using (var context = new MokyklosISEntities())
        {
            if (CheckLoginForContext(context, administrator.Prisijungimo_Vardas, administrator.Slaptazodis) != null)
            {
                newUser.Prisijungimo_Vardas = PickLogin(context, newUser.Vardas, newUser.Pavarde);
                newUser.El_Pastas = string.Empty;
                newUser.Adresas = string.Empty;
                context.Vartotojas.Add(newUser);

                try
                {
 
                    context.SaveChanges();
                    return newUser.Vartotojo_ID;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
        }

        return -1;
    }

    [OperationContract]
    public int Unsafe_ExecuteQueryAndForget(string query)
    {
        using(var connection = GetConnectionContext())
        {
            var command = new SqlCommand(query, connection);
            connection.Open();

            return command.ExecuteNonQuery();
        }
    }

    [OperationContract]
    public Dictionary<string, string> ExecuteQueryReturnFirstRow_SingleParam(string query, string paramName, string param)
    {
        try
        {
            var returnData = new Dictionary<string, string>();

            using (var connection = GetConnectionContext())
            {
                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter(paramName, param));

                connection.Open();

                var reader = command.ExecuteReader();

                if (!reader.HasRows)
                    return returnData;

                reader.Read();

                var row = new Dictionary<string, string>();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string collumnName = reader.GetName(i);
                    string collumnValue = reader[i].ToString();
                    returnData.Add(collumnName, collumnValue);
                }
            }

            return returnData;
        }
        catch (SqlException) { throw; }
    }

    [OperationContract]
    public List<Dictionary<string, string>> ExecuteQueryReturnRows(string query, Dictionary<string, string> parameters)
    {
        try
        {
            var returnData = new List<Dictionary<string, string>>();

            using (var connection = GetConnectionContext())
            {
                var command = new SqlCommand(query, connection);
                connection.Open();

                foreach (var pair in parameters)
                    command.Parameters.Add(new SqlParameter(pair.Key, pair.Value));

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var row = new Dictionary<string, string>();

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string collumnName = reader.GetName(i);
                        string collumnValue = reader[i].ToString();
                        row.Add(collumnName, collumnValue);
                    }

                    returnData.Add(row);
                }
            }

            return returnData;
        }
        catch (SqlException) { throw; }
    }

    [Obsolete]
    [OperationContract]
    public List<Dictionary<string, string>> ExecuteQueryReturnRows_SingleParam(string query, string paramName, string param)
    {
        try
        {
            var returnData = new List<Dictionary<string, string>>();

            using (var connection = GetConnectionContext())
            {
                var command = new SqlCommand(query, connection);
                connection.Open();

                command.Parameters.Add(new SqlParameter(paramName, param));

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var row = new Dictionary<string, string>();

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string collumnName = reader.GetName(i);
                        string collumnValue = reader[i].ToString();
                        row.Add(collumnName, collumnValue);
                    }

                    returnData.Add(row);
                }
            }

            return returnData;
        }
        catch (SqlException) { return null; }
    }

    [Obsolete]
    [OperationContract]
    public List<Dictionary<string, string>> Unsafe_ExecuteQueryReturnRows(string query)
    {
        var returnData = new List<Dictionary<string, string>>();

        using(var connection = GetConnectionContext())
        {
            var command = new SqlCommand(query, connection);
            connection.Open();

            var reader = command.ExecuteReader();

            while(reader.Read())
            {
                var row = new Dictionary<string, string>();

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string collumnName = reader.GetName(i);
                    string collumnValue = reader[i].ToString();
                    row.Add(collumnName, collumnValue);
                }

                returnData.Add(row);
            }
        }

        return returnData;
    }

    [OperationContract]
    public Dictionary<string, List<string>> Unsafe_ExecuteQueryReturnHash(string query)
    {
        var data = new Dictionary<string, List<string>>();

        using (var connection = GetConnectionContext())
        {
            var command = new SqlCommand(query, connection);
            connection.Open();

            var reader = command.ExecuteReader(); 

            for (int i = 0; i < reader.FieldCount; i++)
                data.Add(reader.GetName(i), new List<string>());

            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string collumnName = reader.GetName(i);
                    string collumnValue = reader[i].ToString();
                    data[collumnName].Add(collumnValue);
                }
            }
        }

        return data;
    }
}
