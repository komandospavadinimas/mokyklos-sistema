﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Collections.ObjectModel;

namespace MokyklosSistema.Frames.Registration
{
    public partial class Parent : Page
    {
        public class Representation
        {
            public bool selected { get; set; }
            public int id { get; private set; }
            public string sex { get; private set; }
            public string birthDate { get; private set; }
            public string name { get; private set; }
            public string lastName { get; private set; }

            public Representation(int id, string name, string lastName, string birthDate, string sex)
            {
                this.id = id;
                this.name = name;
                this.lastName = lastName;
                this.birthDate = birthDate;
                this.sex = sex;
            }
        }
        private ObservableCollection<Representation> representation = new ObservableCollection<Representation>();

        public Parent()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Utilities.Security.TestPermissions(Utilities.Ranks_T.Administrator);
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            string queryString = "SELECT TOP 100 * FROM dbo.Vartotojas WHERE dbo.Vartotojas.Pavarde LIKE @LastName AND dbo.Vartotojas.Rangas = 0";

            var db = new MokyklosSistema.DatabaseAccess.DatabaseServiceClient();
            db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
            db.ExecuteQueryReturnRows_SingleParamAsync(queryString, "LastName", lastname.Text);
        }

        void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, DatabaseAccess.ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            var rows = e.Result;
            representation.Clear();

            if(rows != null)
                foreach (var value in rows)
                {
                    var id = int.Parse(value["Vartotojo_ID"]);
                    var name = value["Vardas"];
                    var lastName = value["Pavarde"];
                    var sex = value["Lytis"] == "False" ? "Vyras" : "Moteris";
                    var bday = value["Gimimo_Data"].Substring(0, 10);

                    representation.Add(new Representation(id, name, lastName, bday, sex));
                }

            childrenList.ItemsSource = representation;
        }

        public ObservableCollection<int> GetSelectedChildrenList()
        {
            var list = new ObservableCollection<int>();

            foreach (var child in representation)
                if (child.selected)
                    list.Add(child.id);

            return list;
        }
    }
}
