﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

using System.Windows.Browser;

namespace MokyklosSistema
{
    public partial class MainPage : UserControl
    {
        private StackPanel linksStackPanel;

        private enum ToolbarList_T
        {
            NO_LOGIN,
            STUDENT,
            PARENT,
            TEACHER,
            ADMINISTRATOR,
        }

        public MainPage()
        {
            InitializeComponent();

            var s = Utilities.Security.GetEncodedString("password");

            SetToolBar(ToolbarList_T.NO_LOGIN);

            CreateEventListeners();
        }

        private void SetToolBar(ToolbarList_T type)
        {
            LinksHolder.Children.Clear();

            Page newToolbar;

            switch(type)
            {
                case ToolbarList_T.STUDENT:
                    newToolbar = new Toolbars.Student();
                    linksStackPanel = (newToolbar as Toolbars.Student).LinksStackPanel;
                    break;
                case ToolbarList_T.PARENT:
                    newToolbar = new Toolbars.Parent();
                    linksStackPanel = (newToolbar as Toolbars.Parent).LinksStackPanel;
                    break;
                default:
                case ToolbarList_T.NO_LOGIN:
                    newToolbar = new Toolbars.NoLogin();
                    linksStackPanel = (newToolbar as Toolbars.NoLogin).LinksStackPanel;
                    break;
                case ToolbarList_T.ADMINISTRATOR:
                    newToolbar = new Toolbars.Admin();
                    linksStackPanel = (newToolbar as Toolbars.Admin).LinksStackPanel;
                    break;
                case ToolbarList_T.TEACHER:
                    newToolbar = new Toolbars.Teacher();
                    linksStackPanel = (newToolbar as Toolbars.Teacher).LinksStackPanel;
                    break;
            }

            LinksHolder.Children.Add(newToolbar);
            Grid.SetColumn(newToolbar, 0);
            Grid.SetRow(newToolbar, 0);
        }

        private void CreateEventListeners()
        {
            Session.Instance.OnLogon += OnLogon_User;
            Session.Instance.OnLogoff += OnLogoff_User;
        }

        private void OnLogon_User()
        {
            switch ((Utilities.Ranks_T)Session.Instance.User.Rangas)
            {
                case Utilities.Ranks_T.Administrator:
                    SetToolBar(ToolbarList_T.ADMINISTRATOR);
                    break;
                case Utilities.Ranks_T.Parent:
                    SetToolBar(ToolbarList_T.PARENT);
                    break;
                case Utilities.Ranks_T.Student:
                    SetToolBar(ToolbarList_T.STUDENT);
                    break;
                case Utilities.Ranks_T.Teacher:
                    SetToolBar(ToolbarList_T.TEACHER);
                    break;
            }

            ContentFrame.Navigate(new Uri("/Home", UriKind.Relative));
        }

        private void OnLogoff_User()
        {
            ContentFrame.Navigate(new Uri("/Home", UriKind.Relative));
            SetToolBar(ToolbarList_T.NO_LOGIN);
            CreateEventListeners();
        }

        // After the Frame navigates, ensure the HyperlinkButton representing the current page is selected
        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            if (linksStackPanel != null)
                foreach (UIElement child in linksStackPanel.Children)
                {
                    HyperlinkButton hb = child as HyperlinkButton;
                    if (hb != null && hb.NavigateUri != null)
                    {
                        if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
                        {
                            VisualStateManager.GoToState(hb, "ActiveLink", true);
                        }
                        else
                        {
                            VisualStateManager.GoToState(hb, "InactiveLink", true);
                        }
                    }
                }
        }

        // If an error occurs during navigation, show an error window
        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            ChildWindow errorWin = new ErrorWindow(e.Uri);
            errorWin.Show();
        }

        internal void RedirectNoAccess()
        {
            ContentFrame.Navigate(new Uri("/NoPermission", UriKind.Relative));
        }
    }
}