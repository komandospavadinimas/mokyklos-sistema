﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.IO.IsolatedStorage;

namespace MokyklosSistema.Utilities
{
    public static class Security
    {
        private static SHA256Managed sha = new SHA256Managed();
        private static Random rng = new Random();

        public static string GetEncodedString(string originalString)
        {
            var sha = new SHA256Managed();

            byte[] bytes = Encoding.Unicode.GetBytes(originalString);
            byte[] hash = sha.ComputeHash(bytes);
            string encodedString = string.Empty;

            foreach (byte x in hash)
                encodedString += String.Format("{0:x2}", x);

            return encodedString;
        }

        public static string GetRandomString()
        {
            return GetEncodedString(rng.Next().ToString());
        }

        public static void TestPermissions(short requiredLevel)
        {
            if (!Session.Instance.SessionCreated || Session.Instance.User.Rangas < requiredLevel)
                (Application.Current.RootVisual as MainPage).RedirectNoAccess();
        }

        public static void TestPermissions(Ranks_T requiredLevel)
        {
            if (!Session.Instance.SessionCreated || Session.Instance.User.Rangas < (short)requiredLevel)
                (Application.Current.RootVisual as MainPage).RedirectNoAccess();
        }

        public static void SetDatapackets(Dictionary<string, object> packet, bool markedForDeletion = false)
        {
            var marked = Security.GetDataPackets(false, false, "MarkedForDeletion");

            foreach(var kv in packet)
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(kv.Key))
                    IsolatedStorageSettings.ApplicationSettings[kv.Key] = kv.Value;
                else IsolatedStorageSettings.ApplicationSettings.Add(kv.Key, kv.Value);


                if (markedForDeletion && marked != null)
                {
                    var markedList = marked["MarkedForDeletion"] as List<string>;
                    markedList.Add(kv.Key);
                }
            }
        }

        public static void ClearMarked()
        {
            var marked = Security.GetDataPackets(false, false, "MarkedForDeletion");

            if (marked == null)
                return;

            var markedList = marked["MarkedForDeletion"] as List<string>;

            foreach (var mark in markedList)
                IsolatedStorageSettings.ApplicationSettings.Remove(mark);
        }

        public static void SetDataPacket(string packet, object data, bool markedForDeletion = false)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(packet))
                IsolatedStorageSettings.ApplicationSettings[packet] = data;
            else IsolatedStorageSettings.ApplicationSettings.Add(packet, data);

            if(markedForDeletion)
            {
                var marked = Security.GetDataPackets(false, false, "MarkedForDeletion");

                if (marked != null)
                {
                    var markedList = marked["MarkedForDeletion"] as List<string>;
                    markedList.Add("Attendance_ChildID");
                }
            }
        }

        public static Dictionary<string, object> GetDataPackets(bool deleteData, bool ignoreMissingData, params string[] list)
        {
            var packet = new Dictionary<string, object>();

            foreach(var p in list)
            {
                object data = null;

                if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue(p, out data))
                {
                    if (!ignoreMissingData)
                        return null;
                }
                else if (deleteData)
                    IsolatedStorageSettings.ApplicationSettings.Remove(p);

                packet.Add(p, data);
            }

            return packet;
        }

        public static void DeleteDatapackets(params string[] packet)
        {
            foreach (var p in packet)
                IsolatedStorageSettings.ApplicationSettings.Remove(p);
        }

        public static void DeleteAllPackets()
        {
            IsolatedStorageSettings.ApplicationSettings.Clear();
        }

        public static MainPage GetMainPage()
        {
            return (Application.Current.RootVisual as MainPage);
        }
    }
}
