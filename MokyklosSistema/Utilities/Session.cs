﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.DatabaseAccess;
using System.IO.IsolatedStorage;
using System.Collections.Generic;

namespace MokyklosSistema.Utilities
{
    public class Session
    {
        private static Session instance;
        public bool SessionCreated { get; private set; }
        public Vartotojas User { get; private set; }

        public delegate void LoginInfo();
        public delegate void LogoffInfo();
        public event LoginInfo OnLogon;
        public event LogoffInfo OnLogoff;

        private Session()
        {
            Security.ClearMarked();

            SessionCreated = false;
            User = null;

            var packets = Security.GetDataPackets(false, false, "username", "password");

            if (packets != null)
                TryRestoreSession(packets["username"] as string, packets["password"] as string);
        }

        private void TryRestoreSession(string login, string pass)
        {
            var db = new DatabaseServiceClient();
            db.IsCorrectLoginCompleted += db_IsCorrectLoginCompleted;
            db.IsCorrectLoginAsync(login, pass);
        }

        private void db_IsCorrectLoginCompleted(object sender, IsCorrectLoginCompletedEventArgs e)
        {
            if (e.Result != null)
                CreateSession(e.Result, false);
            else ClearSessionData();

        }

        private void ClearSessionData()
        {
            Security.DeleteDatapackets("username", "password");
        }

        public void DeleteSession()
        {
            ClearSessionData();
            instance = null;

            LogoffInfo handler = OnLogoff;
            if (null != handler) handler();   
        }

        public void CreateSession(Vartotojas user, bool updateSessionData = true)
        {
            this.User = user;
            SessionCreated = true;

            if (updateSessionData)
            {
                ClearSessionData();

                var packet = new Dictionary<string, object>();
                packet.Add("username", user.Prisijungimo_Vardas);
                packet.Add("password", user.Slaptazodis);

                Security.SetDatapackets(packet);
            }

            LoginInfo handler = OnLogon;
            if (null != handler) handler();
        }

        public static Session Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Session();
                }
                return instance;
            }
        }
    }
}
