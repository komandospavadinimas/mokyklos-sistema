﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;
using System.Collections.ObjectModel;
using MokyklosSistema.Views.Popups;

namespace MokyklosSistema.Views
{
    public partial class Attendance : Page
    {
        private TextBlock[] dayArray;
        private UIElementCollection originalChilder;
        private int childID;

        public Attendance()
        {
            InitializeComponent();
            originalChilder = attendanceGrid.Children;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var today = DateTime.Today;

            yearCombo.Items.Add(today.Year);
            yearCombo.SelectedIndex = 0;
            yearCombo.Items.Add(today.Year - 1);
            yearCombo.SelectionChanged += date_SelectionChanged;

            var packet = Security.GetDataPackets(false, false, "Attendance_ChildID");

            if (packet != null)
                childID = packet["Attendance_ChildID"] as int? ?? default(int);

            monthCombo.SelectedIndex = today.Month - 1;
        }

        private void RecreateAttendance()
        {
            foreach (var child in attendanceGrid.Children)
                if (!originalChilder.Contains(child))
                    attendanceGrid.Children.Remove(child);

            var now = DateTime.Now;
            var selectedDate = new DateTime(int.Parse(yearCombo.SelectedItem.ToString()), monthCombo.SelectedIndex + 1, now.Day);
            var days = DateTime.DaysInMonth(selectedDate.Year, selectedDate.Month);

            var beginDate = new DateTime(selectedDate.Year, selectedDate.Month, 1);
            var endDate = new DateTime(selectedDate.Year, selectedDate.Month, days, 23, 59, 59);

            dayArray = new TextBlock[days];

            var db = new DatabaseServiceClient();
            db.GetAttendanceCompleted += db_GetAttendanceCompleted;

            var shift = (int)(new DateTime(selectedDate.Year, selectedDate.Month, 1)).DayOfWeek;
            int j = shift-1, k = 1;

            for (int i = 0; i < days; i++)
            {
                Label dayText = new Label();
                TextBlock text = new TextBlock();
                dayText.Content = (i + 1).ToString();

                if (i == selectedDate.Day - 1 && selectedDate.Year == now.Year && selectedDate.Month == now.Month)
                    dayText.Background = new SolidColorBrush(Color.FromArgb(128, 128, 128, 128));

                text.Text = "\n";
                dayText.VerticalAlignment = System.Windows.VerticalAlignment.Top;

                attendanceGrid.Children.Add(dayText);
                attendanceGrid.Children.Add(text);

                Grid.SetColumn(dayText, j);
                Grid.SetRow(dayText, k);
                Grid.SetColumn(text, j);
                Grid.SetRow(text, k);

                if (++j / 7f == 1)
                {
                    j = 0;
                    k++;
                }

                text.MouseLeftButtonDown += text_MouseLeftButtonDown;
                text.MouseEnter += text_MouseEnter;
                text.MouseLeave += text_MouseLeave;
                text.DataContext = null;

                dayArray[i] = text;
            }

            db.GetAttendanceAsync(childID, beginDate, endDate);
        }

        void text_MouseLeave(object sender, MouseEventArgs e)
        {
            var block = sender as TextBlock;
            block.TextDecorations = null;
        }

        void text_MouseEnter(object sender, MouseEventArgs e)
        {
            var block = sender as TextBlock;
            block.TextDecorations = TextDecorations.Underline;
        }

        void text_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var block = sender as TextBlock;
            var data = block.DataContext as ObservableCollection<Lankomumas>;

            var win = new AttendanceParent(data);
            win.Show();
        }

        void db_GetAttendanceCompleted(object sender, GetAttendanceCompletedEventArgs e)
        {
            var results = e.Result;

            foreach(var record in results)
            {
                var day = record.Data.Day - 1;
                var block = dayArray[day];

                if (block == null)
                    continue;

                if (block.DataContext == null)
                    block.DataContext = new ObservableCollection<Lankomumas>();

                if (record.Ar_Pateisinta == true)
                    dayArray[day].Text += "p ";
                else
                    dayArray[day].Text += "n ";

                (block.DataContext as ObservableCollection<Lankomumas>).Add(record);
            }
        }

        private void date_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RecreateAttendance();
        }

    }
}
