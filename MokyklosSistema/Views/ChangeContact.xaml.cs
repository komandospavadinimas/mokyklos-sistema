﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class ChangeContact : UserControl
    {
        public ChangeContact()
        {
            InitializeComponent();
            Window_Loaded();
        }

        private void Window_Loaded()
        {
            var db = new DatabaseServiceClient();
            db.GetVartotojaCompleted += db_GetVartotojaCompleted;
            db.GetVartotojaAsync(Session.Instance.User.Vartotojo_ID);
        }

        private void db_GetVartotojaCompleted(object sender, GetVartotojaCompletedEventArgs e)
        {
            var results = e.Result;
            ElPastas.Text = results.El_Pastas;
            Telefonas.Text = results.Telefonas.ToString();
            Adresas.Text = results.Adresas;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ChangeEmail();
            ChangePhone();
            ChangeAddress();
            MessageBox.Show("Sėkmingai atnaujinta!");
        }

        private void ChangeEmail()
        {
            var db = new DatabaseServiceClient();
            string queryString = "UPDATE dbo.Vartotojas SET dbo.Vartotojas.El_Pastas = @ElPastas WHERE dbo.Vartotojas.Vartotojo_ID = @SessionId";
            Dictionary<string, string> d = new Dictionary<string, string>()
	        {
	            {"ElPastas", ElPastas.Text},
	            {"SessionId", Session.Instance.User.Vartotojo_ID.ToString()}
	        };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        private void ChangePhone()
        {
            var db = new DatabaseServiceClient();
            string queryString = "UPDATE dbo.Vartotojas SET dbo.Vartotojas.Telefonas = @Telefonas WHERE dbo.Vartotojas.Vartotojo_ID = @SessionId";
            Dictionary<string, string> d = new Dictionary<string, string>()
	        {
	            {"Telefonas", Telefonas.Text},
	            {"SessionId", Session.Instance.User.Vartotojo_ID.ToString()}
	        };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        private void ChangeAddress()
        {
            var db = new DatabaseServiceClient();
            string queryString = "UPDATE dbo.Vartotojas SET dbo.Vartotojas.Adresas = @Adresas WHERE dbo.Vartotojas.Vartotojo_ID = @SessionId";
            Dictionary<string, string> d = new Dictionary<string, string>()
	        {
	            {"Adresas", Adresas.Text},
	            {"SessionId", Session.Instance.User.Vartotojo_ID.ToString()}
	        };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            var results = e.Result;
        }
    }
}
