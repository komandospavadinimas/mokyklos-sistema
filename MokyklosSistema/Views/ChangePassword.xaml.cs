﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class ChangePassword : UserControl
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var KoduotasDabartinisSlapt = Utilities.Security.GetEncodedString(DabartinisSlapt.Password);
            if (KoduotasDabartinisSlapt == Session.Instance.User.Slaptazodis && NaujasSlapt.Password == PakartotasSlapt.Password && NaujasSlapt.Password.Length > 4)
            {
                ChangePassword2();
                MessageBox.Show("Slaptažodis sėkmingai pakeistas!");
            }
            else if (KoduotasDabartinisSlapt != Session.Instance.User.Slaptazodis)
            {
                MessageBox.Show("Blogas dabartinis slaptažodis!");
            }
            else if (NaujasSlapt.Password != PakartotasSlapt.Password)
            {
                MessageBox.Show("Slaptažodžiai nesutampa!");
            }
            else
            {
                MessageBox.Show("Klaida!");
            }
        }

        private void ChangePassword2()
        {
            var db = new DatabaseServiceClient();
            string queryString = "UPDATE dbo.Vartotojas SET dbo.Vartotojas.Slaptazodis = @Slaptazodis WHERE dbo.Vartotojas.Vartotojo_ID = @SessionId";
            Dictionary<string, string> d = new Dictionary<string, string>()
	        {
	            {"Slaptazodis", Utilities.Security.GetEncodedString(NaujasSlapt.Password)},
	            {"SessionId", Session.Instance.User.Vartotojo_ID.ToString()}
	        };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            var results = e.Result;
        }
    }
}
