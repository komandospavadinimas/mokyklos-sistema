﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using MokyklosSistema.DatabaseAccess;
using System.IO.IsolatedStorage;
using MokyklosSistema.Views.Popups;

namespace MokyklosSistema.Views
{

    public partial class LecturePage : Page
    {
        Modulis modulis = null;

        public LecturePage()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            

            IsolatedStorageSettings.ApplicationSettings.TryGetValue("ClassID", out modulis);

            if (modulis != null)
            {
                //kodas
                IsolatedStorageSettings.ApplicationSettings.Remove("ClassID");
                lectureName.Text = modulis.Pavadinimas;
                lectureDescription.Text = modulis.Aprasas;
            }

        }

        private void SaveLectureDescribtion(object sender, RoutedEventArgs e)
        {
            if (modulis != null)
            {
                var db = new DatabaseServiceClient();
                string queryString = "UPDATE dbo.Modulis SET dbo.Modulis.Aprasas = @Aprasas WHERE dbo.Modulis.Modulio_ID = @Modulio_ID";
                Dictionary<string, string> d = new Dictionary<string, string>()
	            {
	                {"Aprasas", lectureDescription.Text},
	                {"Modulio_ID", modulis.Modulio_ID.ToString()}
	            };
                db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
                db.ExecuteQueryReturnRowsAsync(queryString, d);

            }
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            (Application.Current.RootVisual as MainPage).ContentFrame.Navigate(new Uri("/MyLectures", UriKind.Relative));
        }

        private void WriteNotes(object sender, RoutedEventArgs e)
        {
            ChildWindow studentList = new StudentList(modulis.Modulio_ID.ToString(), "WriteNotes");
            studentList.Show();
        }

        private void WriteGrades(object sender, RoutedEventArgs e)
        {
            ChildWindow studentList = new StudentList(modulis.Modulio_ID.ToString(), "WriteGrades");
            studentList.Show();
        }

        private void WriteAttendance(object sender, RoutedEventArgs e)
        {
            ChildWindow studentList = new StudentList(modulis.Modulio_ID.ToString(), "WriteAttendance");
            studentList.Show();
        }

    }
}
