﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class Login : Page
    {
        DatabaseServiceClient db = new DatabaseServiceClient();

        public Login()
        {
            InitializeComponent();

            
            db.IsCorrectLoginCompleted += db_IsCorrectLoginCompleted;
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Click_Login(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.RootVisual as MainPage;

            if (Session.Instance.SessionCreated)
                Session.Instance.DeleteSession();

            db.IsCorrectLoginAsync(login.Text, Utilities.Security.GetEncodedString(password.Password));
        }

        private void db_IsCorrectLoginCompleted(object sender, IsCorrectLoginCompletedEventArgs e)
        {
            if (e.Result != null)
                Session.Instance.CreateSession(e.Result);
            else
                MessageBox.Show("Login failed!");
        }

        private void LayoutRoot_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Click_Login(this,null);
            }
        }
    }
}
