﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;
using MokyklosSistema.Views.Popups;

namespace MokyklosSistema.Views
{
    public partial class MyChildrenList : Page
    {
        public MyChildrenList()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var db = new DatabaseServiceClient();
            db.GetChildrenListCompleted += db_GetChildrenListCompleted;
            db.GetChildrenListAsync(Session.Instance.User.Vartotojo_ID);
        }

        void db_GetChildrenListCompleted(object sender, GetChildrenListCompletedEventArgs e)
        {
            childrenDatagrid.ItemsSource = e.Result;
        }

        private void ChildrenList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (childrenDatagrid.SelectedItem == null)
                return;

            var child = childrenDatagrid.SelectedItem as Vartotojas;

            var opt = new Parent_ChildOptions(child);
            opt.Show();
            opt.Closed += opt_Closed;
        }

        void opt_Closed(object sender, EventArgs e)
        {
            var opt = sender as Parent_ChildOptions;

            if (opt.DialogResult == false)
                childrenDatagrid.SelectedItem = -1;
        }
    }
}
