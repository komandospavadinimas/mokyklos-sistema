﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class MyClasses : Page
    {
        private Vartotojas student;

        public MyClasses()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var packet = Security.GetDataPackets(true, false, "ClassInfo_StudentOverride");

            student = packet == null ? Session.Instance.User : packet["ClassInfo_StudentOverride"] as Vartotojas;

            string query = "SELECT * FROM dbo.Modulis WHERE Modulio_ID = ANY (SELECT Modulio_ID FROM dbo.Mokiniai_Moduliai WHERE Mokinio_ID = @StudentID)";

            var db = new DatabaseServiceClient();
            db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
            db.ExecuteQueryReturnRows_SingleParamAsync(query, "studentID", student.Vartotojo_ID.ToString());
        }

        void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            var list = new List<Modulis>();

            foreach(var c in e.Result)
            {
                var module = new Modulis();
                module.Modulio_ID = int.Parse(c["Modulio_ID"]);
                module.Pavadinimas = c["Pavadinimas"];
                module.Aprasas = c["Aprasas"];

                list.Add(module);
            }

            moduleGrid.ItemsSource = list;
        }

        private void moduleGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (moduleGrid.SelectedItem == null) return;

            var module = moduleGrid.SelectedItem as Modulis;

            var classInfoWin = new ClassInfo(module.Modulio_ID, student.Vartotojo_ID);
            classInfoWin.Show();

            classInfoWin.Closed += classInfoWin_Closed;
        }

        void classInfoWin_Closed(object sender, EventArgs e)
        {
            moduleGrid.SelectedItem = -1;
        }
    }
}
