﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using MokyklosSistema.Utilities;
using MokyklosSistema.DatabaseAccess;
using System.IO.IsolatedStorage;

namespace MokyklosSistema.Views
{
    public partial class MyLectures : Page
    {
       
        public MyLectures()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Utilities.Security.TestPermissions(Ranks_T.Teacher);
            
            var db = new DatabaseServiceClient();

            db.GetModuliaiCompleted += db_GetModuliaiCompleted;
            db.GetModuliaiAsync(Session.Instance.User.Vartotojo_ID);
        }

        void db_GetModuliaiCompleted(object sender, GetModuliaiCompletedEventArgs e)
        {
            PamokosGrid.ItemsSource = e.Result;
        }

        private void SelectModule(object sender, SelectionChangedEventArgs e)
        {
            object temp;
            if (IsolatedStorageSettings.ApplicationSettings.TryGetValue("ClassID", out temp))
                IsolatedStorageSettings.ApplicationSettings.Remove("ClassID");

            IsolatedStorageSettings.ApplicationSettings.Add("ClassID", ((Modulis) PamokosGrid.SelectedItem));
            (Application.Current.RootVisual as MainPage).ContentFrame.Navigate(new Uri("/LecturePage", UriKind.Relative));
        }

    }
}
