﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class MyProfile : UserControl
    {
        public MyProfile()
        {
            InitializeComponent();
            Window_Loaded();
        }

        private void Window_Loaded()
        {
            var db = new DatabaseServiceClient();
            db.GetVartotojaCompleted += db_GetVartotojaCompleted;
            db.GetVartotojaAsync(Session.Instance.User.Vartotojo_ID);
        }

        private void db_GetVartotojaCompleted(object sender, GetVartotojaCompletedEventArgs e)
        {
            var results = e.Result;
            TextBlockPrisijungimoVardas.Text = results.Prisijungimo_Vardas;
            TextBlockVardas.Text = results.Vardas;
            TextBlockPavarde.Text = results.Pavarde;
            TextBlockLytis.Text = results.Lytis == true ? "Moteris" : "Vyras";
            TextBlockElPastas.Text = results.El_Pastas;
            TextBlockTelefonas.Text = results.Telefonas.ToString();
            TextBlockGimimoData.Text = results.Gimimo_Data.ToShortDateString();
            TextBlockAdresas.Text = results.Adresas;

            var db = new DatabaseServiceClient();
            switch (results.Rangas)
            {
                case 0:
                    Papildomaa_Tekstas1.Text = "Grupė:";
                    Papildoma1.Text = "Mokinys";

                    string query2 = "SELECT Klases_Pavadinimas, Kabinetas, Istojimo_Data FROM dbo.Mokinys, dbo.Klase WHERE Vartotojo_ID = @VartotojoID AND dbo.Klase.Klases_ID = dbo.Mokinys.Klases_ID";
                    
                    db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted2;
                    db.ExecuteQueryReturnRows_SingleParamAsync(query2, "VartotojoID", Session.Instance.User.Vartotojo_ID.ToString());

                    break;
                case 10:
                    Papildomaa_Tekstas1.Text = "Grupė:";
                    Papildoma1.Text = "Tėvas";
                    break;
                case 20:
                    Papildomaa_Tekstas1.Text = "Grupė:";
                    Papildoma1.Text = "Mokytojas";
                    
                    string query = "SELECT Idarbinimo_Data, Kabinetas FROM dbo.Mokytojas WHERE Vartotojo_ID = @VartotojoID";
                    
                    db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
                    db.ExecuteQueryReturnRows_SingleParamAsync(query, "VartotojoID", Session.Instance.User.Vartotojo_ID.ToString());
                   
                    break;
                case 50:
                    Papildomaa_Tekstas1.Text = "Grupė:";
                    Papildoma1.Text = "Administratorius";
                    break;
                default:
                    break;
            }

        }

        private void db_ExecuteQueryReturnRows_SingleParamCompleted2(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                var results = e.Result[0];
                Papildomaa_Tekstas2.Text = "Įstojimo data:";
                Papildoma2.Text = results["Istojimo_Data"];
                Papildomaa_Tekstas3.Text = "Klasės pavadinimas:";
                Papildoma3.Text = results["Klases_Pavadinimas"];
                Papildomaa_Tekstas4.Text = "Klasės kabinetas:";
                Papildoma4.Text = results["Kabinetas"];
            }
        }

        private void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                var results = e.Result[0];
                Papildomaa_Tekstas2.Text = "Įdarbinimo data:";
                Papildoma2.Text = results["Idarbinimo_Data"];
                Papildomaa_Tekstas3.Text = "Kabinetas:";
                Papildoma3.Text = results["Kabinetas"];
            }
        }
    }
}
