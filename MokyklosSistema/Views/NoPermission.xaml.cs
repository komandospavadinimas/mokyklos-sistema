﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace MokyklosSistema.Views
{
    public partial class NoPermission : Page
    {
        private DispatcherTimer redirectTimer;
        public NoPermission()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            redirectTimer = new System.Windows.Threading.DispatcherTimer();
            redirectTimer.Interval = new TimeSpan(0, 0, 0, 3, 0);

            redirectTimer.Tick += redirectTimer_Tick;
            redirectTimer.Start();
        }

        private void redirectTimer_Tick(object sender, EventArgs e)
        {
            redirectTimer.Stop();
            (Application.Current.RootVisual as MainPage).ContentFrame.Navigate(new Uri("/Home", UriKind.Relative));
        }
    }
}
