﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

using System.Windows.Threading;
using System.Windows.Browser;

namespace MokyklosSistema.Views
{
    public partial class Note : Page
    {
        public Note()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var packet = Security.GetDataPackets(true, false, "ChildInfo_ID");

            if (packet == null) return;

            var childID = packet["ChildInfo_ID"] as int?;

            string query = "SELECT Pavadinimas, Mokinio_ID, Pastabos_ID, Data, Tema, Pranesimas FROM dbo.Pastaba, dbo.Modulis, dbo.Tevai_Vaikai WHERE Tevo_ID = @TevoID AND Vaiko_ID = @VaikoId AND Vaiko_ID = Mokinio_ID AND dbo.Pastaba.Modulio_ID = dbo.Modulis.Modulio_ID";

            var db = new DatabaseServiceClient();
            Dictionary<string, string> d = new Dictionary<string, string>()
	        {
	            {"TevoID", Session.Instance.User.Vartotojo_ID.ToString()},
	            {"VaikoId", childID.ToString()}
	        };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(query, d);
        }

        public class View
        {
            public int NoteID { get; set; }
            public string Caption { get; set; }
            public string ModuleName { get; set; }
            public string Date { get; set; }
        }

        void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            var list = new List<View>();

            foreach (var pm in e.Result)
            {
                var representation = new View();

                representation.NoteID = int.Parse(pm["Pastabos_ID"]);
                representation.Caption = pm["Tema"];
                representation.ModuleName = pm["Pavadinimas"];
                representation.Date = pm["Data"];
                list.Add(representation);
            }

            noteGrid.ItemsSource = list;

            /*
             
             <ScrollViewer x:Name="ClassList" HorizontalAlignment="Stretch"  Margin="10,10,10,10" VerticalAlignment="Stretch" ScrollViewer.HorizontalScrollBarVisibility="Auto" ScrollViewer.VerticalScrollBarVisibility="Auto">
            <StackPanel x:Name="classStack"/>
        </ScrollViewer>
             * 
             * 
             var pm = e.Result;
            {
                var holder = new Grid();
                var col1 = new ColumnDefinition();
                var col2 = new ColumnDefinition();
                var col3 = new ColumnDefinition();
                var col4 = new ColumnDefinition();

                col2.MaxWidth = classStack.ActualWidth / 5;
                col3.MaxWidth = classStack.ActualWidth / 8;
                col4.MaxWidth = classStack.ActualWidth / 10;

                holder.ColumnDefinitions.Add(col1);
                holder.ColumnDefinitions.Add(col2);
                holder.ColumnDefinitions.Add(col3);
                holder.ColumnDefinitions.Add(col4);
                classStack.Children.Add(holder);

                var text = new TextBox();
                var text3 = new TextBox();
                var text4 = new TextBox();

                text.Text = "Pastabos tema";
                text3.Text = "Modulio pavadinimas";
                text4.Text = "Pastabos data";

                text.IsReadOnly = text3.IsReadOnly = text4.IsReadOnly = true;
                text.TextAlignment = text3.TextAlignment = text4.TextAlignment = TextAlignment.Center;

                holder.Children.Add(text);
                holder.Children.Add(text3);
                holder.Children.Add(text4);

                Grid.SetColumn(text, 0);
                Grid.SetColumn(text3, 1);
                Grid.SetColumn(text4, 2);
            }

            foreach (var c in pm)
            {
                var holder = new Grid();
                var col1 = new ColumnDefinition();
                var col2 = new ColumnDefinition();
                var col3 = new ColumnDefinition();
                var col4 = new ColumnDefinition();

                col2.MaxWidth = classStack.ActualWidth / 5;
                col3.MaxWidth = classStack.ActualWidth / 8;
                col4.MaxWidth = classStack.ActualWidth / 10;

                holder.ColumnDefinitions.Add(col1);
                holder.ColumnDefinitions.Add(col2);
                holder.ColumnDefinitions.Add(col3);
                holder.ColumnDefinitions.Add(col4);
                classStack.Children.Add(holder);

                var text = new TextBox();
                var text3 = new TextBox();
                var text4 = new TextBox();
                var button = new Button();

                button.CommandParameter = c["Pastabos_ID"];
                button.Click += PmButton_Click;
                text.Text = c["Tema"];
                text3.Text = c["Pavadinimas"];
                text4.Text = c["Data"];
                button.Content = "Peržiūrėti";

                text.IsReadOnly = text3.IsReadOnly = text4.IsReadOnly = true;

                holder.Children.Add(text);
                holder.Children.Add(text3);
                holder.Children.Add(text4);
                holder.Children.Add(button);

                Grid.SetColumn(text, 0);
                Grid.SetColumn(text3, 1);
                Grid.SetColumn(text4, 2);
                Grid.SetColumn(button, 3);
            }*/
        }

        void PmButton_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var command = button.CommandParameter;

            ChildWindow errorWin = new NoteInfo(command.ToString(), Session.Instance.User.Vartotojo_ID.ToString());
            errorWin.Show();
        }

        private void noteGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (noteGrid.SelectedItem == null)
                return;

            var note = noteGrid.SelectedItem as View;

            var opt = new NoteInfo(note.NoteID.ToString(), Utilities.Session.Instance.User.Vartotojo_ID.ToString());
            opt.Show();
            opt.Closed += note_Closed;
        }

        private void note_Closed(object sender, EventArgs e)
        {
            noteGrid.SelectedItem = -1;
        }

    }
}
