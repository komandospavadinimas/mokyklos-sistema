﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

using System.Windows.Threading;
using System.Windows.Browser;

namespace MokyklosSistema.Views
{
    public partial class PM : Page
    {

        public PM()
        {
            InitializeComponent();
            Window_Loaded();
        }

        public void Window_Loaded()
        {
            string query = "SELECT Pranesimo_ID, Prisijungimo_Vardas, Data, Pranesimas1, Tema FROM dbo.Pranesimas, dbo.Vartotojas WHERE Gavejo_ID = @GavejoID AND Siuntejo_ID = Vartotojo_ID";

            var db = new DatabaseServiceClient();
            db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
            db.ExecuteQueryReturnRows_SingleParamAsync(query, "GavejoID", Session.Instance.User.Vartotojo_ID.ToString());
        }

        void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            var pm = e.Result;

            {
                var holder = new Grid();
                var col1 = new ColumnDefinition();
                var col2 = new ColumnDefinition();
                var col3 = new ColumnDefinition();
                var col4 = new ColumnDefinition();
                var col5 = new ColumnDefinition();

                col2.MaxWidth = classStack.ActualWidth / 5;
                col3.MaxWidth = classStack.ActualWidth / 5;
                col4.MaxWidth = classStack.ActualWidth / 10;
                col5.MaxWidth = classStack.ActualWidth / 10;

                holder.ColumnDefinitions.Add(col1);
                holder.ColumnDefinitions.Add(col2);
                holder.ColumnDefinitions.Add(col3);
                holder.ColumnDefinitions.Add(col4);
                holder.ColumnDefinitions.Add(col5);
                classStack.Children.Add(holder);

                var text = new TextBox();
                var text2 = new TextBox();
                var text3 = new TextBox();

                text.Text = "Pranešimo tema";
                text2.Text = "Siuntėjas";
                text3.Text = "Siuntimo data";

                text.IsReadOnly = text2.IsReadOnly = text3.IsReadOnly = true;
                text.TextAlignment = text2.TextAlignment = text3.TextAlignment = TextAlignment.Center;

                holder.Children.Add(text);
                holder.Children.Add(text2);
                holder.Children.Add(text3);

                Grid.SetColumn(text, 0);
                Grid.SetColumn(text2, 1);
                Grid.SetColumn(text3, 2);
            }

            foreach (var c in pm)
            {
                var holder = new Grid();
                var col1 = new ColumnDefinition();
                var col2 = new ColumnDefinition();
                var col3 = new ColumnDefinition();
                var col4 = new ColumnDefinition();
                var col5 = new ColumnDefinition();

                col2.MaxWidth = classStack.ActualWidth / 5;
                col3.MaxWidth = classStack.ActualWidth / 5;
                col4.MaxWidth = classStack.ActualWidth / 10;
                col5.MaxWidth = classStack.ActualWidth / 10;

                holder.ColumnDefinitions.Add(col1);
                holder.ColumnDefinitions.Add(col2);
                holder.ColumnDefinitions.Add(col3);
                holder.ColumnDefinitions.Add(col4);
                holder.ColumnDefinitions.Add(col5);
                classStack.Children.Add(holder);

                var text = new TextBox();
                var text2 = new TextBox();
                var text3 = new TextBox();
                var button = new Button();
                var button2 = new Button();

                button.CommandParameter = c["Pranesimo_ID"];
                button.Click += PmButton_Click;
                button2.CommandParameter = c["Pranesimo_ID"];
                button2.Click += PmDeleteButton_Click;
                text.Text = c["Tema"];
                text2.Text = c["Prisijungimo_Vardas"];
                text3.Text = c["Data"];
                button.Content = "Peržiūrėti";
                button2.Content = "Ištrinti";

                text.IsReadOnly = text2.IsReadOnly = text3.IsReadOnly = true;

                holder.Children.Add(text);
                holder.Children.Add(text2);
                holder.Children.Add(text3);
                holder.Children.Add(button);
                holder.Children.Add(button2);

                Grid.SetColumn(text, 0);
                Grid.SetColumn(text2, 1);
                Grid.SetColumn(text3, 2);
                Grid.SetColumn(button, 3);
                Grid.SetColumn(button2, 4);
            }
        }

        private void PmDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var command = button.CommandParameter;

            string query = "DELETE FROM dbo.Pranesimas WHERE Pranesimo_ID = @PranesimoID";

            var db = new DatabaseServiceClient();
            db.ExecuteQueryReturnRows_SingleParamAsync(query, "PranesimoID", command.ToString());

            (Application.Current.RootVisual as MainPage).ContentFrame.Refresh();
        }

        void PmButton_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var command = button.CommandParameter;

            ChildWindow errorWin = new PMInfo(command.ToString(), Session.Instance.User.Vartotojo_ID.ToString());
            errorWin.Show();
        }

    }
}
