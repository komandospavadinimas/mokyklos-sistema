﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class AddAttendance : ChildWindow
    {
        private string moduleID;
        private string childrenID;

        public AddAttendance(string moduleID, string childrenID, ObservableCollection<Lankomumas> dayList)
        {
            InitializeComponent();
            this.moduleID = moduleID;
            this.childrenID = childrenID;
        }

        private bool Add(string moduleID, string childrenID)
        {
            
            {
                var db = new DatabaseServiceClient();

                var attendance = new Lankomumas();
                attendance.Data = DateTime.Now;
                attendance.Mokytojo_ID = Utilities.Session.Instance.User.Vartotojo_ID;
                attendance.Modulio_ID = int.Parse(moduleID);
                //attendance.Ar_Pateisinta = ;
                attendance.Mokinio_ID = int.Parse(childrenID);

                db.AddAttendanceCompleted += db_QueryCompleted;
                db.AddAttendanceAsync(attendance);
                return true;
            }
        }

        private void db_QueryCompleted(object sender, AddAttendanceCompletedEventArgs e)
        {
            if (e.Result <= 0)
                MessageBox.Show("Klaida, pastaba nepridėta!");
            else this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Add(moduleID, childrenID);
        }

    }
}

