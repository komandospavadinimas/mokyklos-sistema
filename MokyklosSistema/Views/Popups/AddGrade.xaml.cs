﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class AddGrade : ChildWindow
    {
        private string moduleID;
        private string childrenID;

        public AddGrade(string moduleID, string childrenID)
        {
            InitializeComponent();
            this.moduleID = moduleID;
            this.childrenID = childrenID;
        }

        private bool Add(string moduleID, string childrenID)
        {
            var newGrade = 0;

            if (!int.TryParse(gradeTextbox.Text, out newGrade))
                return false;

            if (1 <= newGrade && newGrade <= 10)
            {
                var db = new DatabaseServiceClient();

                var grade = new Pazymys();
                grade.Data = DateTime.Now;
                grade.Mokytojo_ID = Utilities.Session.Instance.User.Vartotojo_ID;
                grade.Modulio_ID = int.Parse(moduleID);
                grade.Pazymys1 = int.Parse(gradeTextbox.Text);
                grade.Mokinio_ID = int.Parse(childrenID);
                grade.Komentaras = Komentaras.Text;

                db.AddGradeCompleted += db_QueryCompleted;
                db.AddGradeAsync(grade);
                return true;
            }
            else
            {
                MessageBox.Show("Pažymiai gali būti nuo 1 iki 10!");
                return false;
            }
        }

        private void db_QueryCompleted(object sender, AddGradeCompletedEventArgs e)
        {
            if (e.Result <= 0)
                MessageBox.Show("Klaida, pažymys nepridėtas!");
            else this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Add(moduleID, childrenID);
        }

    }
}

