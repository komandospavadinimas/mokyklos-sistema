﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class AddNote : ChildWindow
    {
        private string moduleID;
        private string childrenID;

        public AddNote(string moduleID, string childrenID)
        {
            InitializeComponent();
            this.moduleID = moduleID;
            this.childrenID = childrenID;
        }

        private bool Add(string moduleID, string childrenID)
        {
            if(!string.IsNullOrEmpty(Tema.Text) && !string.IsNullOrEmpty(Pranesimas.Text) )
            {
                var db = new DatabaseServiceClient();

                var note = new Pastaba();
                note.Data = DateTime.Now;
                note.Mokytojo_ID = Utilities.Session.Instance.User.Vartotojo_ID;
                note.Modulio_ID = int.Parse(moduleID);
                note.Tema = Tema.Text;
                note.Mokinio_ID = int.Parse(childrenID);
                note.Pranesimas = Pranesimas.Text;

                db.AddNoteCompleted += db_QueryCompleted;
                db.AddNoteAsync(note);
                return true;
            }
            else
            {
                MessageBox.Show("Užpildyk visus laukus!");
                return false;
            }
        }

        private void db_QueryCompleted(object sender, AddNoteCompletedEventArgs e)
        {
            if (e.Result <= 0)
                MessageBox.Show("Klaida, pastaba nepridėta!");
            else this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Add(moduleID, childrenID);
        }

    }
}

