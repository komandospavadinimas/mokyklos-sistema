﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.DatabaseAccess;
using System.Collections.ObjectModel;

namespace MokyklosSistema.Views.Popups
{
    public partial class AttendanceParent : ChildWindow
    {
        public class View
        {
            public int AttendanceID { get; set; }
            public string Lesson { get; set; }
            public bool Justified { get; set; }
            public string Comment { get; set; }
        }

        private ObservableCollection<Lankomumas> dayRecords;
        private ObservableCollection<View> viewRecords;

        public AttendanceParent(ObservableCollection<Lankomumas> dayList)
        {
            InitializeComponent();

            if (dayList != null && dayList.Count > 0)
            {
                this.dayRecords = dayList;

                var db = new DatabaseServiceClient();
                db.GetClassNamesCompleted += db_GetClassNamesCompleted;
                db.GetClassNamesAsync(dayList);
            }
        }

        void db_GetClassNamesCompleted(object sender, GetClassNamesCompletedEventArgs e)
        {
            viewRecords = new ObservableCollection<View>();

            foreach(var record in dayRecords)
            {
                var view = new View();
                view.AttendanceID = record.Modulio_ID;
                view.Comment = record.Komentaras;
                view.Justified = record.Ar_Pateisinta ?? false;
                
                string lesson;

                if (!e.Result.TryGetValue(record.Modulio_ID, out lesson))
                    lesson = string.Empty;

                view.Lesson = lesson;
                viewRecords.Add(view);
            }

            attendanceGrid.ItemsSource = viewRecords;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (dayRecords == null || dayRecords.Count <= 0)
            {
                this.DialogResult = true;
                return;
            }

            foreach(var view in viewRecords)
            {
                var record = dayRecords.FirstOrDefault(o => o.Iraso_ID == view.AttendanceID);

                if (record == null)
                    continue;

                record.Komentaras = view.Comment;
                record.Ar_Pateisinta = view.Justified;
            }

            var db = new DatabaseServiceClient();
            db.SaveAttendanceRecordsAsync(dayRecords);

            this.DialogResult = true;
        }
    }
}

