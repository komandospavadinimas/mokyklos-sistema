﻿using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class ChildrenAttendance : ChildWindow
    {
        private TextBlock[] dayArray;
        private string moduleID;
        private string childrenID;

        public ChildrenAttendance(string moduleID, string childrenID)
        {
            InitializeComponent();

            AttendanceList(moduleID, childrenID);

            this.moduleID = moduleID;
            this.childrenID = childrenID;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        public void AttendanceList(string moduleID, string childrenID)
        {
            var db = new DatabaseServiceClient();

            var childID = int.Parse(childrenID);

            var now = DateTime.Now;
            var days = DateTime.DaysInMonth(now.Year, now.Month);

            var beginDate = new DateTime(now.Year, now.Month, 1);
            var endDate = new DateTime(now.Year, now.Month, days, 23, 59, 59);

            dayArray = new TextBlock[days];

            db.GetAttendanceCompleted += db_GetAttendanceCompleted;

            for (int i = 0; i < days; i++)
            {
                TextBlock text = new TextBlock();
                text.Text = (i + 1).ToString();
                attendanceGrid.Children.Add(text);

                Grid.SetColumn(text, i % 7);
                Grid.SetRow(text, i / 7);

                text.MouseLeftButtonDown += text_MouseLeftButtonDown;
                text.DataContext = null;

                dayArray[i] = text;
            }

            db.GetAttendanceAsync(childID, beginDate, endDate);
        }

        void text_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var block = sender as TextBlock;
            var data = block.DataContext as ObservableCollection<Lankomumas>;

            var win = new AddAttendance(moduleID, childrenID, data);
            win.Show();

            win.Closed += opt_Closed;
        }

        void opt_Closed(object sender, EventArgs e)
        {
            if ((sender as AddAttendance).DialogResult == true)
            {
                attendanceGrid.Children.Clear();
                AttendanceList(moduleID, childrenID);
            }
        }

        void db_GetAttendanceCompleted(object sender, GetAttendanceCompletedEventArgs e)
        {
            var results = e.Result;

            foreach (var record in results)
            {
                var day = record.Data.Day - 1;
                var block = dayArray[day];

                if (block == null)
                    continue;

                if (block.DataContext == null)
                {
                    block.TextDecorations = TextDecorations.Underline;
                    block.DataContext = new ObservableCollection<Lankomumas>();
                }

                dayArray[day].Text += " n";
                (block.DataContext as ObservableCollection<Lankomumas>).Add(record);
            }
        }
    }
}

