﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class ChildrenGradeList : ChildWindow
    {
        private string moduleID;
        private string childrenID;
        private ObservableCollection<View> gradeListView = new ObservableCollection<View>();

        public ChildrenGradeList(string moduleID, string childrenID)
        {
            InitializeComponent();
            GradeList(moduleID, childrenID);

            this.moduleID = moduleID;
            this.childrenID = childrenID;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void GradeList(string moduleID, string childrenID)
        {
            var db = new DatabaseServiceClient();
            string queryString = "SELECT Modulio_ID, Mokinio_ID, Pazymys1, Data, Komentaras FROM dbo.Pazymys WHERE Mokinio_ID = @Mokinio_ID AND Modulio_ID = @Modulio_ID AND Mokytojo_ID = @Mokytojo_ID ORDER BY Data DESC, Pazymys1 DESC";
            Dictionary<string, string> d = new Dictionary<string, string>()
	            {
                    {"Mokytojo_ID", Utilities.Session.Instance.User.Vartotojo_ID.ToString()},
	                {"Modulio_ID", moduleID},
                    {"Mokinio_ID", childrenID}
	            };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        public class View
        {
            public string Modulio_ID { get; set; }
            public string Mokinio_ID { get; set; }
            public string Pazymys { get; set; }
            public string Data { get; set; }
            public string Komentaras { get; set; }
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            foreach (var grade in e.Result)
            {
                var representation = new View();
                representation.Modulio_ID = grade["Modulio_ID"];
                representation.Mokinio_ID = grade["Mokinio_ID"];
                representation.Pazymys = grade["Pazymys1"];
                representation.Data = grade["Data"].Substring(0, 10);
                representation.Komentaras = grade["Komentaras"];
                gradeListView.Add(representation);
            }

            gradeDataGrid.ItemsSource = gradeListView;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var opt = new AddGrade(moduleID, childrenID);
            opt.Show();

            opt.Closed += opt_Closed;
        }

        void opt_Closed(object sender, EventArgs e)
        {
            if ((sender as AddGrade).DialogResult == true)
                GradeList(moduleID, childrenID);
        }

    }
}

