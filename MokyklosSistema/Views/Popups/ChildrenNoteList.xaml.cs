﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class ChildrenNoteList : ChildWindow
    {
        private string moduleID;
        private string childrenID;
        private ObservableCollection<View> noteListView = new ObservableCollection<View>();

        public ChildrenNoteList(string moduleID, string childrenID)
        {
            InitializeComponent();
            NoteList(moduleID, childrenID);

            this.moduleID = moduleID;
            this.childrenID = childrenID;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void NoteList(string moduleID, string childrenID)
        {
            var db = new DatabaseServiceClient();
            string queryString = "SELECT Modulio_ID, Mokinio_ID, Tema, Data, Pranesimas FROM dbo.Pastaba WHERE Mokinio_ID = @Mokinio_ID AND Modulio_ID = @Modulio_ID AND Mokytojo_ID = @Mokytojo_ID ORDER BY Data DESC";
            Dictionary<string, string> d = new Dictionary<string, string>()
	            {
                    {"Mokytojo_ID", Utilities.Session.Instance.User.Vartotojo_ID.ToString()},
	                {"Modulio_ID", moduleID},
                    {"Mokinio_ID", childrenID}
	            };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        public class View
        {
            public string Modulio_ID { get; set; }
            public string Mokinio_ID { get; set; }
            public string Tema { get; set; }
            public string Data { get; set; }
            public string Pranesimas { get; set; }
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            foreach (var note in e.Result)
            {
                var representation = new View();
                representation.Modulio_ID = note["Modulio_ID"];
                representation.Mokinio_ID = note["Mokinio_ID"];
                representation.Tema = note["Tema"];
                representation.Data = note["Data"].Substring(0, 10);
                representation.Pranesimas = note["Pranesimas"];
                noteListView.Add(representation);
            }

            noteDataGrid.ItemsSource = noteListView;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var opt = new AddNote(moduleID, childrenID);
            opt.Show();

            opt.Closed += opt_Closed;
        }

        void opt_Closed(object sender, EventArgs e)
        {
            if ((sender as AddNote).DialogResult == true)
            {
                noteListView.Clear();
                NoteList(moduleID, childrenID);
            }
        }

    }
}

