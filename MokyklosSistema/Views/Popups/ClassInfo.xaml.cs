﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Linq;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Views;
using System.Collections.Generic;

namespace MokyklosSistema
{
    public partial class ClassInfo : ChildWindow
    {
        public ClassInfo(int classID, int childId)
        {
            InitializeComponent();

            string dataQuery = "SELECT * FROM dbo.Modulis WHERE Modulio_ID= @ClassID";
            string teacherQuery = "SELECT TOP 1 Vardas, Pavarde FROM dbo.Vartotojas WHERE Vartotojo_ID = (SELECT TOP 1 Mokytojo_ID FROM dbo.Mokytojai_Moduliai WHERE Modulio_ID=@ClassID)";

            var db = new DatabaseServiceClient();

            db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
            db.GetGradesCompleted += db_GetGradesCompleted;
            db.ExecuteQueryReturnFirstRow_SingleParamCompleted += db_ExecuteQueryReturnFirstRow_SingleParamCompleted;

            db.ExecuteQueryReturnRows_SingleParamAsync(dataQuery, "ClassID", classID.ToString());
            db.ExecuteQueryReturnFirstRow_SingleParamAsync(teacherQuery, "ClassID", classID.ToString());
            db.GetGradesAsync(childId, classID);
        }

        void db_ExecuteQueryReturnFirstRow_SingleParamCompleted(object sender, ExecuteQueryReturnFirstRow_SingleParamCompletedEventArgs e)
        {
            string name, lastName = string.Empty;

            e.Result.TryGetValue("Vardas", out name);
            e.Result.TryGetValue("Pavarde", out lastName);

            teacherName.Text = name + " " + lastName;
        }

        void db_GetGradesCompleted(object sender, GetGradesCompletedEventArgs e)
        {
            gradeList.ItemsSource = e.Result;

            float? sum = e.Result.Sum(x => x.Pazymys1);
            average.Text = (sum / e.Result.Count).ToString();
        }

        private void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                var data = e.Result[0];
                className.Text = data["Pavadinimas"];
                classInfo.Text = data["Aprasas"];
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}