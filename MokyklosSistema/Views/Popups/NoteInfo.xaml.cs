﻿using System;
using System.Windows;
using System.Windows.Controls;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Views;
using System.Collections.Generic;

namespace MokyklosSistema
{
    public partial class NoteInfo : ChildWindow
    {
        public NoteInfo(string noteID, string parentID)
        {
            InitializeComponent();

            string query = "SELECT Vardas, Pavarde, Data, Tema, Pranesimas FROM dbo.Pastaba, dbo.Vartotojas WHERE Pastabos_ID = @PastabosID AND Mokytojo_ID = Vartotojo_ID";

            var db = new DatabaseServiceClient();
            db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
            db.ExecuteQueryReturnRows_SingleParamAsync(query, "PastabosID", noteID);
        }

        private void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                var data = e.Result[0];
                Tema.Text = data["Tema"];
                Pranesimas.Text = data["Pranesimas"];
                Mokytojas.Text = data["Vardas"] + " " + data["Pavarde"];
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}