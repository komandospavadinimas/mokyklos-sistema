﻿using System;
using System.Windows;
using System.Windows.Controls;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Views;
using System.Collections.Generic;

namespace MokyklosSistema
{
    public partial class PMInfo : ChildWindow
    {
        public PMInfo(string pranesimoID, string childId)
        {
            InitializeComponent();

            string query = "SELECT * FROM dbo.Pranesimas WHERE Pranesimo_ID = @PranesimoID";

            var db = new DatabaseServiceClient();
            db.ExecuteQueryReturnRows_SingleParamCompleted += db_ExecuteQueryReturnRows_SingleParamCompleted;
            db.ExecuteQueryReturnRows_SingleParamAsync(query, "PranesimoID", pranesimoID);
        }

        private void db_ExecuteQueryReturnRows_SingleParamCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                var data = e.Result[0];
                Tema.Text = data["Tema"];
                Pranesimas.Text = data["Pranesimas1"];

                var db = new DatabaseServiceClient();
                string query = "SELECT Prisijungimo_Vardas FROM dbo.Vartotojas WHERE Vartotojo_ID = @SiuntejoID";
                db.ExecuteQueryReturnRows_SingleParamCompleted += db_QueryCompleted;
                db.ExecuteQueryReturnRows_SingleParamAsync(query, "SiuntejoID", data["Siuntejo_ID"]);
            }
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                var data = e.Result[0];
                Siuntejas.Text = data["Prisijungimo_Vardas"];
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}