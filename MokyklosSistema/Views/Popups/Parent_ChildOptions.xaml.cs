﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views.Popups
{
    public partial class Parent_ChildOptions : ChildWindow
    {
        private readonly string managementString = " valdymas";
        private readonly Vartotojas child;

        public Parent_ChildOptions(Vartotojas child)
        {
            InitializeComponent();

            this.child = child;

            if (child != null)
                Title = child.Vardas + " " + child.Pavarde + " " + managementString;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void GradesButton_Click(object sender, RoutedEventArgs e)
        {
            Security.SetDataPacket("ClassInfo_StudentOverride", child);
            Security.GetMainPage().ContentFrame.Navigate(new Uri("/MyClasses", UriKind.Relative));
            DialogResult = true;
        }

        private void NotesButton_Click(object sender, RoutedEventArgs e)
        {
            Security.SetDataPacket("ChildInfo_ID", child.Vartotojo_ID);
            Security.GetMainPage().ContentFrame.Navigate(new Uri("/Note", UriKind.Relative));
            DialogResult = true;
        }

        private void AttendanceButton_Click(object sender, RoutedEventArgs e)
        {
            Security.SetDataPacket("Attendance_ChildID", child.Vartotojo_ID, true);

            Security.GetMainPage().ContentFrame.Navigate(new Uri("/Attendance", UriKind.Relative));
            DialogResult = true;
        }
    }
}

