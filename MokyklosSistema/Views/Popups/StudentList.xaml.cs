﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MokyklosSistema.Views.Popups
{
    public partial class StudentList : ChildWindow
    {
        private string pageName;
        public StudentList(string moduleID, string pageName)
        {
            InitializeComponent();

            this.pageName = pageName;
            PopulateList(moduleID);
        }

        private void PopulateList(string moduleID)
        {
            var db = new DatabaseServiceClient();
            string queryString = "SELECT dbo.Mokiniai_Moduliai.Modulio_ID, dbo.Vartotojas.Vartotojo_ID, Vardas, Pavarde, Klases_Pavadinimas FROM dbo.Mokiniai_Moduliai, dbo.Vartotojas, dbo.Klase, dbo.Mokinys WHERE dbo.Mokiniai_Moduliai.Modulio_ID = @Modulio_ID AND dbo.Mokiniai_Moduliai.Mokinio_ID = dbo.Mokinys.Vartotojo_ID AND dbo.Mokinys.Klases_ID = dbo.Klase.Klases_ID AND dbo.Mokinys.Vartotojo_ID = dbo.Vartotojas.Vartotojo_ID";
            Dictionary<string, string> d = new Dictionary<string, string>()
	            {
	                {"Modulio_ID", moduleID}
	            };
            db.ExecuteQueryReturnRowsCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRowsAsync(queryString, d);
        }

        public class View
        {
            public string Modulio_ID { get; set; }
            public string Vartotojo_ID { get; set; }
            public string Vardas { get; set; }
            public string Pavarde { get; set; }
            public string KlasesPavadinimas { get; set; }
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRowsCompletedEventArgs e)
        {
            var list = new List<View>();

            foreach (var student in e.Result)
            {
                var representation = new View();

                representation.Modulio_ID = student["Modulio_ID"];
                representation.Vartotojo_ID = student["Vartotojo_ID"];
                representation.Vardas = student["Vardas"];
                representation.Pavarde = student["Pavarde"];
                representation.KlasesPavadinimas = student["Klases_Pavadinimas"];
                list.Add(representation);
            }

            studentList.ItemsSource = list;
        }

        private void StudentList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (studentList.SelectedItem == null)
                return;

            var children = studentList.SelectedItem as View;

            switch (pageName)
            {
                case "WriteNotes":
                    var opt = new ChildrenNoteList(children.Modulio_ID.ToString(), children.Vartotojo_ID.ToString());
                    opt.Show();
                    opt.Closed += note_Closed;
                    break;
                case "WriteGrades":
                    var opt2 = new ChildrenGradeList(children.Modulio_ID.ToString(), children.Vartotojo_ID.ToString());
                    opt2.Show();
                    opt2.Closed += note_Closed;
                    break;
                case "WriteAttendance":
                    var opt3 = new ChildrenAttendance(children.Modulio_ID.ToString(), children.Vartotojo_ID.ToString());
                    opt3.Show();
                    opt3.Closed += note_Closed;
                    break;
                default:
                    break;
            }
            
        }

        private void note_Closed(object sender, EventArgs e)
        {
            studentList.SelectedItem = -1;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}

