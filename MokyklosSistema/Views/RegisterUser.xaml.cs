﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;

using MokyklosSistema.DatabaseAccess;
using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class RegisterUser : Page
    {
        private Page extraSettings;
        
        private const string adminString = "Administratorius";
        private const string teacherString = "Mokytojas";
        private const string parentString = "Tėvas";
        private const string studentString = "Mokinys";
        private const string maleString = "Vyras";
        private const string femaleString = "Moteris";

        public RegisterUser()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Utilities.Security.TestPermissions(Ranks_T.Administrator);
        }

        public static short GetRankFromString(string rank)
        {
            switch(rank)
            {
                case adminString:
                    return (short)Ranks_T.Administrator;

                case teacherString:
                    return (short)Ranks_T.Teacher;

                case parentString:
                    return (short)Ranks_T.Parent;

                case studentString:
                default:
                    return (short)Ranks_T.Student;
            }
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            if (name.Text.Length < 5 || lastName.Text.Length < 5 || birthday.SelectedDate == null)
            {
                MessageBox.Show("Error");
                return;
            }

            var selectedRank = ((ComboBoxItem)rank.SelectedItem).Content.ToString();

            Vartotojas user = new Vartotojas();
            user.Gimimo_Data = birthday.SelectedDate ?? DateTime.Now;
            user.Lytis = (((ComboBoxItem)sex.SelectedItem).Content.ToString() == maleString ? false : true);
            user.Pavarde = lastName.Text;
            user.Vardas = name.Text;
            user.Rangas = GetRankFromString(selectedRank);
            user.Slaptazodis = Utilities.Security.GetEncodedString("password");
            user.Registracijos_Data = DateTime.Now;

            var db = new DatabaseServiceClient();

            switch (selectedRank)
            {
                case studentString:
                    var student = new Mokinys();
                    student.Istojimo_Data = user.Registracijos_Data;
                    student.Klases_ID = 1;
                    db.CreateStudentCompleted += db_CreateStudentCompleted;
                    db.CreateStudentAsync(Session.Instance.User, user, student);
                    break;

                default:
                    db.CreateUserCompleted += db_CreateUserCompleted;
                    db.CreateUserAsync(Session.Instance.User, user);
                    break;

                case parentString:
                    db.CreateParentCompleted += db_CreateParentCompleted;
                    db.CreateParentAsync(Session.Instance.User, user, (extraSettings as Frames.Registration.Parent).GetSelectedChildrenList());
                    break;
            }
        }

        void db_CreateStudentCompleted(object sender, CreateStudentCompletedEventArgs e)
        {
            MessageBox.Show(e.Result != -1 ? "created" : "failed");
        }

        void db_CreateParentCompleted(object sender, CreateParentCompletedEventArgs e)
        {
            MessageBox.Show(e.Result != -1 ? "created" : "failed");
        }

        void db_CreateUserCompleted(object sender, CreateUserCompletedEventArgs e)
        {
            MessageBox.Show(e.Result != -1 ? "created" : "failed");
        }

        private void Rank_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (extraSettingsFrame != null)
                extraSettingsFrame.Children.Clear();

            if (rank != null)
                switch (((ComboBoxItem)rank.SelectedItem).Content.ToString())
                {
                    case parentString:
                        extraSettings = new Frames.Registration.Parent();
                        break;

                    default:
                        extraSettings = null;
                        break;
                }

            if(extraSettings != null)
            {
                extraSettingsFrame.Children.Add(extraSettings);
                Grid.SetColumn(extraSettings, 0);
                Grid.SetRow(extraSettings, 0);
            }
        }

    }
}
