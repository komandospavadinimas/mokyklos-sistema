﻿using MokyklosSistema.DatabaseAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using MokyklosSistema.Utilities;

namespace MokyklosSistema.Views
{
    public partial class SendPM : UserControl
    {
        public SendPM()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Send_PM();
        }

        private void Send_PM()
        {
            var db = new DatabaseServiceClient();
            string query = "SELECT Vartotojo_Id FROM dbo.Vartotojas WHERE Prisijungimo_Vardas = @GavejoVardas";
            db.ExecuteQueryReturnRows_SingleParamCompleted += db_QueryCompleted;
            db.ExecuteQueryReturnRows_SingleParamAsync(query, "GavejoVardas", Gavejas.Text);
        }

        private void db_QueryCompleted(object sender, ExecuteQueryReturnRows_SingleParamCompletedEventArgs e)
        {
            if (e.Result.Count > 0 && int.Parse(e.Result[0]["Vartotojo_Id"]) != Session.Instance.User.Vartotojo_ID)
            {
                var db = new DatabaseServiceClient();

                var msg = new Pranesimas();
                msg.Data = DateTime.Now;
                msg.Siuntejo_ID = Session.Instance.User.Vartotojo_ID;
                msg.Tema = Tema.Text;
                msg.Pranesimas1 = Pranesimas.Text;

                msg.Gavejo_ID = int.Parse(e.Result[0]["Vartotojo_Id"]);

                db.CreateMessageCompleted += db_QueryCompleted;
                db.CreateMessageAsync(msg);
            }
            else
                MessageBox.Show("Klaida, nėra tokio vartotojo!");
        }

        private void db_QueryCompleted(object sender, CreateMessageCompletedEventArgs e)
        {
            if (e.Result > 0)
                MessageBox.Show("Sėkmingai išsiuntėte žinutę!");
            else
                MessageBox.Show("Klaida, žinutė neišsiųstą!");
        }

    }
}
