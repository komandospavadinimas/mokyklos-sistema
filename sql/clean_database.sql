﻿DELETE FROM dbo.Klase;
DBCC CHECKIDENT (Klase, RESEED, 0);

DELETE FROM dbo.Lankomumas;
DBCC CHECKIDENT (Lankomumas, RESEED, 0);

DELETE FROM dbo.Modulis;
DBCC CHECKIDENT (Modulis, RESEED, 0);

DELETE FROM dbo.Mokinys;
DBCC CHECKIDENT (Mokinys, RESEED, 0);

DELETE FROM dbo.Mokiniai_Moduliai;
DBCC CHECKIDENT (Mokiniai_Moduliai, RESEED, 0);

DELETE FROM dbo.Mokytojas;
DBCC CHECKIDENT (Mokytojas, RESEED, 0);

DELETE FROM dbo.Mokytojai_Moduliai;
DBCC CHECKIDENT (Mokytojai_Moduliai, RESEED, 0);

DELETE FROM dbo.Pastaba;
DBCC CHECKIDENT (Pastaba, RESEED, 0);

DELETE FROM dbo.Pazymys;
DBCC CHECKIDENT (Pazymys, RESEED, 0);

DELETE FROM dbo.Pranesimas;
DBCC CHECKIDENT (Modulis, RESEED, 0);

DELETE FROM dbo.Tevai;
DBCC CHECKIDENT (Tevai, RESEED, 0);

DELETE FROM dbo.Tevai_Vaikai;
DBCC CHECKIDENT (Tevai_Vaikai, RESEED, 0);

DELETE FROM dbo.Vartotojas;
DBCC CHECKIDENT (Vartotojas, RESEED, 0);